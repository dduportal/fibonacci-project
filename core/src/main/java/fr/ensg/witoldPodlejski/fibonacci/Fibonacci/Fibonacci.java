package fr.ensg.witoldPodlejski.fibonacci.Fibonacci;



public class Fibonacci{

	public static int compute(int nb) {
		if (nb < 1) {
			return 0;
		} else {
			int n1 = 0;
			int n2 = 1;
			int counter = 1;
			while (counter < nb) {
				int temp = n2;
				n2 = n1 + n2;
				n1 = temp;
				counter += 1;
			}
			return n2;
		}

	}

}

