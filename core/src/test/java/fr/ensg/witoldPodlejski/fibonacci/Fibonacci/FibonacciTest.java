package fr.ensg.witoldPodlejski.fibonacci.Fibonacci;

import static org.junit.Assert.*;
import org.junit.Test;


public class FibonacciTest {

	@Test
	public void test() {
		assertEquals(0, Fibonacci.compute(-1));
		assertEquals(0, Fibonacci.compute(0));
		assertEquals(1, Fibonacci.compute(1));
		assertEquals(233, Fibonacci.compute(13));

	}

}

