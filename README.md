
# Projet Maven/Integration continue

Durant ce projet il fallait mettre en place une application java de calculs de la suite de fibonacci avec maven et deployer une interface d'integration continue.

## Partie Maven
Tout c'est relativement bien déroulé, je pense avoir remplit les conditions du sujet, le **maven site** affiche des erreurs mais termine correctement.

## Partie integration continue avec **Gitlab CI**

### Ou je me suis arrété

Cette partie à été plus compliqué pour moi, j'ai passé les cinq heures et je me suis arrété à l'état actuel du dévellopement. 
J'ai assez bien compris comment continuer mais je n'arrive pas à effectuer un **maven site**, le maven install fonctionne mais pas build ni le site sur le **contener docker**. 
Je n'ai pas plus de pistes pour debugger le projet le problème étant que maven ne reconner pas les dépendances internes au projet, peut-être s'agit-il des options maven.

### Ce qui a été fait

Le stage build est en théorie fonctionnel, par exemple un **mvn clean install** fonctionne. L'étape test est bonne avec les deux executions en parallèle, on notera qu'il n'y a pas de test d'integration mais qu'ils seraient executés si il y en avait.
Le stage deploy sur l'image  docker **busybox** devrait marcher et mettre en ligne le site pour la branch master.

### Ce qu'il faudrait faire

Il faudrait ajouter un site pour d'autres branches que master et gérer les **pull requests**.
Cela n'a pas été fait par manque de temps et l'impossibilité de tester puisque le pipeline se stope à la première étape.

