package fr.ensg.witoldPodlejski.maven.plugins.fibonacci;


import org.springframework.web.bind.annotation.*;
import fr.ensg.witoldPodlejski.fibonacci.Fibonacci.Fibonacci;


@RestController
public class HomeController {

    @GetMapping(value="/fibonacci/{rank}")
    public String fibonacci(@PathVariable int rank) {

        return Integer.toString(Fibonacci.compute(rank)) + "\n";
    }
}
